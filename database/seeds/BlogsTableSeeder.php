<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

       DB::table('blogs')->insert(array(
		array(
				'title' => 'laravel',
        		'description' => 'Laravel is a web application framework with expressive, elegant syntax. We’ve already laid the foundation — freeing you to create without sweating the small things.',
                'image' =>"IMG_20191214_165018.jpg",
		),
		array(
				'title' => 'Composer',
        		'description' => 'Composer is an application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries.',
                  'image' =>"IMG_20191214_17131.jpg",
		),
        array(
                'title' => 'Laracasts',
                'description' => 'Nine out of ten doctors recommend Laracasts over competing brands. Check them out, see for yourself, and massively level up your development skills in the process.',
                  'image' =>"IMG_20191214_171649.jpg",
        ),
        array(
                'title' => 'what',
                'description' => 'However, if you are not using Homestead, you will need to make sure your server meets the following requirements:',
                  'image' =>"IMG_20191214_171331.jpg",
        ),
        array(
                'title' => 'news',
                'description' => 'Want to create a Laravel project with login, registration, and more features already built for you? ',
                  'image' =>"IMG_20191214_171656.jpg",
        ),
        array(
                'title' => 'blog',
                'description' => 'If you have PHP installed locally and you would like to use PHP',
                'image' =>"IMG_20191214_171331.jpg",
        ),
        array(
                'title' => 'public',
                'description' => 'After installing Laravel, you should configure your web server',
                'image' =>"IMG_20191214_171331.jpg",
        ),
        array(
                'title' => 'web',
                'description' => 'All of the configuration files for the Laravel framework are stored in the config directory.',
                  'image' =>"IMG_20191214_172526.jpg",
        ),
        array(
                'title' => 'each',
                'description' => 'each option is documented, so feel free to look through the files and get familiar with the options available to you.',
                  'image' =>"IMG_20191214_172526.jpg",
        ),
        array(
                'title' => 'Application Key',
                'description' => 'The next thing you should do after installing Laravel is set your application key to a random string.',
                  'image' =>"IMG_20191214_171656.jpg",
        ),
        array(
                'title' => 'file',
                'description' => 'Typically, this string should be 32 characters long. The key can be set in the .env environment file.',
                  'image' =>"IMG_20191214_171656.jpg",
        ),
        array(
                'title' => 'config',
                'description' => 'It contains several options such as timezone and locale that you may wish to change according to your application.',
                  'image' =>"IMG_20191214_165018.jpg",
        ),
       
		));

    }
}
