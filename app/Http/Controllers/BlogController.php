<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Blog;
use App\Comment;
use Validator;
use Redirect;

class BlogController extends Controller
{
    //
    public function index()
    {
    	$blog = Blog::paginate(10);
    	return view('blog.index',['blog'=>$blog]);
    }

    public function view($id)
    {
    	$blog = Blog::where('id',$id)->first();
    	return view('blog.view',['blog'=>$blog]);
    }

    public function store(Request $request)
    {	
    	if (Auth::check()){
    		$validator=Validator::make($request->all(),[
            'comment' => 'required|min:3',
        	]);

	        if($validator->fails())
	        {	
	           return redirect::back()->withErrors($validator)->withInput();
	        }else{
	                $cmt = new Comment();
	                $id = Auth::user()->id;
	                $cmt->user_id = $id;
	                $cmt->blog_id = $request->blog_id;
	                $cmt->comment = $request->comment;
	                $cmt->save();
	                return back()->with('success','Comment add successfully!');
	            }

    	}else
		{ 
			return back()->with('error','You have not logged in!');;
		}
    }
}
