<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blogs</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
  </head>
  <body>
    <div class="row">
      <div class="col-lg-6"> 
          <table class="table table-bordered" style="text-align:center;">
              <thead>
                <tr class="table-danger">
                  <td><b>No</b></td>
                  <td><b>Title</b></td>
                  <td><b>Image</b></td>
                  <td><b>Action</b></td>  
                </tr>
              </thead>
              <tbody>
                @foreach($blog as $key=>$blogs)
                <tr>
                  <td>{{($blog->currentpage()-1) * $blog->perpage() + $key + 1}}</td>
                  <td>{{$blogs->title}}</td>
                  <td><img src="{{asset('image/'.$blogs->image)}}"  style="width:50px;height:50px"/></td>
                  <td><a href="{{route('blogview',$blogs->id)}}" class="btn btn-white"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
                @endforeach
              </tbody>
          </table>
      </div>
    </div>
    {!! $blog->links() !!}
  </body>
</html>