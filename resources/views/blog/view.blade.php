<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<title>Blog</title>
</head>
<body>
	<div class="card">
	  	<div class="card-body">
	        <div class="col-md-12">
	            <form id="comment" method="post" action="{{route('comment')}}">
	            	@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>	
						        <strong>{{ $message }}</strong>
						</div>
					@endif


					@if ($message = Session::get('error'))
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>	
						        <strong>{{ $message }}</strong>
						</div>
					@endif
			        @csrf
			        <input type="hidden" name="blog_id" id="blog_id" value="{{$blog->id}}">
					<div class="row">
					    <div class="col-lg-6" >
					   		<div>
						        <label for="title"><b>Title</b></label>
							</div>
					    	<div>
					    		 <textarea class="form-control" name="title" id="title" rows="3" readonly="readonly">{{ $blog->title }}</textarea>
				            </div>
				        </div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div>
						        <label for="description"><b>Description</b></label>
							</div>
				            <div >
				                <textarea class="form-control" name="description" id="description" rows="3" readonly="readonly">{{ $blog->description }}</textarea>
				            </div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div>
								<label for="profile"><b>Profile</b></label>
							</div>
				            <div >
                       			 <img src="{{asset('image/'.$blog->image)}}"  style="width:150px;height:150px"/>
				            </div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div>
						        <label for="comment"><b>Comment</b></label>
						   </div>
				           <div>    
						    	<textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
			  				</div>
							 <span class="text-danger"><b>{{ $errors->first('comment') }}</b></span>
						</div>
					</div>
					<div class="row" style="padding-top: 10px;">
						<div class="col-lg-6">
							<button type="submit" class="btn btn-success" id="viewSaveDoc">
			                        {{__('Save')}}
			                    </button>
			                <a href="{{route('blogs')}}" class="btn btn-primary pull-right">
                                               {{__('Back')}} </a>
						</div>
					</div>
		        </form>
	        </div>
		</div>
	</div>	    	
</body>
</html>